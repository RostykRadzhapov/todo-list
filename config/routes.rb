Rails.application.routes.draw do
  devise_for :users
    root to: 'lists#index'

    resources :lists
    resources :items

    get 'complete', to: 'items#comlete'
    get 'incomplete', to: 'items#incomlete'
end
