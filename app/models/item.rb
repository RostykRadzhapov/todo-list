class Item < ApplicationRecord
  belongs_to :list

  scope :complete, -> { where(done: true) }
  scope :incomplete, -> { where(done: false) }

  validates :name, presence: true
end
