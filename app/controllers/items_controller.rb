class ItemsController < ApplicationController
  def index
    @items = Item.all
  end

  def show
    @item = Item.find(params[:id])
  end

  def create
    @list = List.find(params[:item][:list_id])
    @item = @list.items.create(item_params)
    redirect_to list_path(@list)
  end

  def edit
    @item = Item.find(params[:id])
  end

  def update
    @item = Item.find(params[:id])
    list_id = @item.list_id
    if @item.update(item_params)
      redirect_to list_path(list_id)
    else
      render 'edit'
    end
  end

  def destroy
    @item = Item.find(params[:id])
    list_id = @item.list_id
    @item.destroy

    redirect_to list_path(list_id)
  end

  def comlete
    @item = Item.find(params[:id])
    list_id = @item.list_id
    @item.update(done: true)

    redirect_to list_path(list_id)
  end

  def incomlete
    @item = Item.find(params[:id])
    list_id = @item.list_id
    @item.update(done: false)

    redirect_to list_path(list_id)
  end

  private def item_params
    params.require(:item).permit(:name, :description)
  end
end
