class ListsController < ApplicationController
  before_action :authenticate_user!

  def index
    @lists = current_user.lists
  end

  def show
    @list = List.find(params[:id])
    @item = @list.items.new
  end

  def new
    @list = current_user.lists.build
  end

  def create
    @list = current_user.lists.build(lists_params)

    if @list.save
      redirect_to @list
    else
      render 'new'
    end
  end

  def edit
    @list = List.find(params[:id])
  end

  def update
    @list = List.find(params[:id])

    if @list.update(lists_params)
      redirect_to lists_path
    else
      render 'edit'
    end
  end

  def destroy
    @list = List.find(params[:id])
    @list.destroy

    redirect_to lists_path
  end

  private

  def lists_params
    params.require(:list).permit(:name)
  end
end
